class Parent {
        void fun1(){

                print("In fun1 - Demo1");
        }
}
mixin Demo on Parent{ 

        void fun2(){
                print(" In fun2 Demo2 ");
        }
}
class Normal with Demo {

}
void main(){
        Normal obj = new Normal();
        obj.fun1();
        obj.fun2();
}
