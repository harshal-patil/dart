mixin DemoParent { 
	void fun1(){

		print("In fun1 - Demo1");
	}
}
class Demo{

	void fun2(){ 
		print(" In fun2 Demo2 ");  
	}
}
class Normal extends Demo with DemoParent {

}
void main(){
	Normal obj = new Normal();
	obj.fun1();
	obj.fun2();
}
