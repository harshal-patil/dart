mixin Demo1 {
	int x=10;
        void fun1(){

                print("In fun1 - Demo1");
        }
	void fun2();
}
class ChildDemo with Demo1{

        void fun2(){
                print(" In fun2 Demo2 ");
		print("$x");
        }
}

void main(){
        ChildDemo obj = new ChildDemo();
        obj.fun1();
        obj.fun2();
}
