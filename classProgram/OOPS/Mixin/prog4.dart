abstract class Demo1 {
        void fun1(){

                print("In fun1 - Demo1");
        }void fun2();
}
abstract class Demo2{

        void fun3(){
                print(" In fun3 Demo2 ");
        }
	void fun4();
}
class Normal implements Demo1,Demo2 {
	void fun2(){
		print("IN fun2");
	}
	void fun4(){
		print("in fun4");
	}
}
void main(){
        Normal obj = new Normal();
        obj.fun1();
        obj.fun2();
	obj.fun3();
	obj.fun4();
}
