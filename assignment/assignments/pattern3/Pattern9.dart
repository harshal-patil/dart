import 'dart:io';

void main() {
  int n = 5;
  int x = 1;
  int a = x;
  for (int i = 1; i <= n; i++) {
    a = x;
    for (int j = 1; j <= i; j++) {
      stdout.write("$a ");
      a = a + i;
    }
    x = x + 2;
    print("");
  }
}
