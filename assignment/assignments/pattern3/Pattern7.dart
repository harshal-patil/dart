import 'dart:io';

void main() {
  int n = 4;
  int x = 1;
  int a = 1;
  for (int i = 1; i <= n; i++) {
    a = x;
    for (int j = 1; j <= i; j++) {
      stdout.write("$a ");
      a = a + 2;
    }
    x = x + 2;
    print("");
  }
}
