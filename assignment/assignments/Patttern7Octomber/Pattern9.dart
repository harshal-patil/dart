import 'dart:io';

void main() {
  int n = 3;
  int a = 1;
  for (int i = 1; i <= n; i++) {
    int a = i;
    for (int j = 1; j <= n; j++) {
      stdout.write("$a ");
      a = a + 2;
    }
    print("");
  }
}
