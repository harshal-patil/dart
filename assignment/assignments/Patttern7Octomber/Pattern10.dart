import 'dart:io';

void main() {
  int n = 4;
  int x = 1;
  for (int i = 1; i <= n; i++) {
    int a = x;
    for (int j = 1; j <= n; j++) {
      if (j == 2) {
        x = a;
      }
      stdout.write("$a ");
      a = a + (i + 1);
    }
    print("");
  }
}
